﻿using StackExchange.Redis;
using Microsoft.AspNetCore.SignalR;
using Topic2Chat.ChatHub;
using Topic2Chat.Models;

namespace Topic2Chat.Extensions
{
    public static class RedisExtensions
    {
        //for flushing database
        private static bool _firstTimeRunningFlushDatabase = true;
        private static object _lockFlushDatabase = new object();

        //for subscribeing to chats
        private static bool _firstTimeRunningAllChatsSubscribe = true;
        private static object _lockAllChatsSubscribe = new object();

        //for subscribeing to comment on chats
        private static bool _firstTimeRunningSingleChatSubscribe = true;
        private static object _lockSingleChatSubscribe = new object();

        public static void FlushRedisDatabase(this IConnectionMultiplexer _redis)
        {
            lock (_lockFlushDatabase)
            {
                if (!_firstTimeRunningFlushDatabase)
                    return;

                _redis.GetServer("localhost:6379").FlushAllDatabasesAsync();

                _firstTimeRunningFlushDatabase = false;
            }
        }

        public static void SubscribeToNotificationToNewChatRooms(this IConnectionMultiplexer _redis,
            string chatChannel,
            IHubContext<Topic2ChatHub> hub)
        {
            lock (_lockAllChatsSubscribe)
            {
                if (!_firstTimeRunningAllChatsSubscribe)
                    return;

                _firstTimeRunningAllChatsSubscribe = false;

                _redis.GetSubscriber().Subscribe(chatChannel, async (chanel, message) =>
                {
                    await hub.Clients.All.SendAsync("GroupChat", (string)message);
                });
            }
        }

        public static void SubscrirbeToAllChatRooms(this IConnectionMultiplexer _redis,
            string chatChannel,
            IHubContext<Topic2ChatHub> hub)
        {
            lock (_lockSingleChatSubscribe)
            {
                if (!_firstTimeRunningSingleChatSubscribe)
                    return;

                _firstTimeRunningSingleChatSubscribe = false;

                _redis.GetSubscriber().Subscribe(chatChannel, async (chanel, message) =>
                {
                    await hub.Clients.All.SendAsync("SingleGroupChat", (string)message);
                });
            }
        }
    }
}