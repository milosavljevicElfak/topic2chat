﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Topic2Chat.Session
{
    public static class SessionConfig
    {
        public static string Username => "_sessionUserName";

        public static string CurrentGroupChat => "_sessionCurrentGroupChat";
    }
}
