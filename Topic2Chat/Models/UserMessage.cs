﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Topic2Chat.Models
{
    public class UserMessage
    {
        public string ToChat { get; set; }
        public string User { get; set; }
        public string MessageContent { get; set; }
        public string WhenMessagePosted => DateTime.Now.ToString("dd.MM.yyyy - HH:mm");
        
    }
}
