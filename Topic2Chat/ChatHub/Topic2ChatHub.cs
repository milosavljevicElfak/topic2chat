﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Topic2Chat.ChatHub
{
    public class Topic2ChatHub : Hub
    {
        public Task SendMessage(string room, string user, string message)
        {
            return Clients.Group(room).SendAsync("Send", user, message);
        }

        public async Task AddToGroup(string user, string groupName)
        {
            await Groups.AddToGroupAsync(user, groupName);

            await Clients.Group(groupName).SendAsync("Send", $"{user} has joined the group {groupName}.");
        }

        public async Task RemoveFromGroup(string user, string groupName)
        {
            await Groups.RemoveFromGroupAsync(user, groupName);

            await Clients.Group(groupName).SendAsync("Send", $"{user} has left the group {groupName}.");
        }
    }
}
