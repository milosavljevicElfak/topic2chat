﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Topic2Chat.Models;
using Topic2Chat.Extensions;
using Topic2Chat.Session;
using StackExchange.Redis;
using Newtonsoft.Json;
using Microsoft.AspNetCore.SignalR;
using Topic2Chat.ChatHub;
using System.Collections.Generic;

namespace Topic2Chat.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConnectionMultiplexer _redis;
        private readonly IHubContext<Topic2ChatHub> _hub;

        private readonly string _topic2ChatKey = "Topic2Chat";
        private readonly string _usernamesKey;
        private readonly string _groupChatNamesKey;
        private readonly string _newChatChanelRoomNotificationRedisChanel = "SignalR.NewChatChannelRoomNotification";
        private readonly string _allChatRoomsRedisChanel = "SignalR.AllChatRooms";
        

        public HomeController(IHubContext<Topic2ChatHub> hub, IConnectionMultiplexer redis)
        {
            //default local host port is 6379
            _usernamesKey = $"{_topic2ChatKey}.Usernames";
            _groupChatNamesKey = $"{_topic2ChatKey}.GroupChatNames";

            _redis = redis;            
            _hub = hub;

            _redis.FlushRedisDatabase();
            _redis.SubscribeToNotificationToNewChatRooms(_newChatChanelRoomNotificationRedisChanel, _hub);
            _redis.SubscrirbeToAllChatRooms(_allChatRoomsRedisChanel, _hub);


        }

        public IActionResult Index()
        {
            string username = HttpContext.Session.Get<string>(SessionConfig.Username);
            if(string.IsNullOrWhiteSpace(username))
                return View();  
            return RedirectToAction("GroupList");
        }

        [HttpPost]
        public async Task<IActionResult> Index(string username)
        {
            if(string.IsNullOrEmpty(username))
                return View();

            var listOfSelectedUsernames = await _redis.GetDatabase().ListRangeAsync(_usernamesKey);
            if (listOfSelectedUsernames.Contains(username))
                return View();

            await _redis.GetDatabase().ListRightPushAsync(_usernamesKey, username);
            HttpContext.Session.Set<string>(SessionConfig.Username, username);

            return RedirectToAction("GroupList");
        }

        public async Task<IActionResult> GroupList()
        {
            string username = HttpContext.Session.Get<string>(SessionConfig.Username);
            if(string.IsNullOrWhiteSpace(username))
                return RedirectToAction("Index"); 

            string previousGroupChat = HttpContext.Session.Get<string>(SessionConfig.CurrentGroupChat);

            if (!string.IsNullOrEmpty(previousGroupChat))
            {
                UserMessage userHasLeft = new UserMessage
                {
                    MessageContent = "This user has left this chat",
                    User = username,
                    ToChat = previousGroupChat
                };
                
                string jsonLeftMessage = JsonConvert.SerializeObject(userHasLeft);

                await _redis.GetSubscriber().PublishAsync(_allChatRoomsRedisChanel , jsonLeftMessage);
            }

            IList<string> groupChats = (await _redis.GetDatabase().ListRangeAsync(_groupChatNamesKey))
                .Select(gc => gc.ToString()).ToList() ?? new List<string>();
            return View(groupChats);
        }

        public async Task<IActionResult> Group(string groupChat)
        {        
            string username = HttpContext.Session.Get<string>(SessionConfig.Username);
            
            
            if(string.IsNullOrWhiteSpace(username))
                return RedirectToAction("Index"); 
            
            string smallCaseGroupChat = groupChat.ToLower();
            var groupChats = await _redis.GetDatabase().ListRangeAsync(_groupChatNamesKey);
            if (!groupChats.Contains(smallCaseGroupChat))
                return RedirectToAction("GroupList");

            HttpContext.Session.Set(SessionConfig.CurrentGroupChat, smallCaseGroupChat);

            UserMessage userHasJoined = new UserMessage
            {
                MessageContent = "This user has joined this chat",
                ToChat = smallCaseGroupChat,
                User = username
            };

            string jsonJoinedMessage = JsonConvert.SerializeObject(userHasJoined);

            await _redis.GetSubscriber().PublishAsync(_allChatRoomsRedisChanel , jsonJoinedMessage);

            return View(model: smallCaseGroupChat);
        }

        [HttpPost]
        public async Task<IActionResult> Comment(string message, string groupChat)
        {
            var username = HttpContext.Session.Get<string>(SessionConfig.Username);
            if(string.IsNullOrWhiteSpace(username))
                return Unauthorized();

            if(string.IsNullOrWhiteSpace(groupChat))
                return BadRequest();

            if(string.IsNullOrWhiteSpace(message))
                return BadRequest();

            var smallCaseGroupChat = groupChat.ToLower();
            var groupChats = await _redis.GetDatabase().ListRangeAsync(_groupChatNamesKey);
            if (!groupChats.Contains(smallCaseGroupChat))
                return BadRequest();

            var userMessage = new UserMessage
            {
                MessageContent = message,
                User = username,
                ToChat = smallCaseGroupChat
            };

            var userMessageJson = JsonConvert.SerializeObject(userMessage);

            _redis.GetSubscriber().Publish(_allChatRoomsRedisChanel, userMessageJson);

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddGroupChat(string groupChat)
        {
            string username = HttpContext.Session.Get<string>(SessionConfig.Username);
            if(string.IsNullOrWhiteSpace(username))
                return RedirectToAction("Index");

            string smallCaseGroupChat = groupChat.ToLower();
            var groupChats = await _redis.GetDatabase().ListRangeAsync(_groupChatNamesKey);
            if (string.IsNullOrEmpty(smallCaseGroupChat) || groupChats.Contains(smallCaseGroupChat))
                return RedirectToAction("GroupList");

            await _redis.GetDatabase().ListRightPushAsync(_groupChatNamesKey, smallCaseGroupChat);
            await _redis.GetSubscriber().PublishAsync(_newChatChanelRoomNotificationRedisChanel, smallCaseGroupChat);

            return RedirectToAction("GroupList");
        }
    }
}
