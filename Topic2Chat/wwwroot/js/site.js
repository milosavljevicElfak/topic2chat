﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$('#add-room-btn').click((event) => {
	event.preventDefault();
	submitRoom();
});

function submitRoom() {
	const roomName = $('#room-input').val;
	if (roomName === '') return;

	$.ajax({
		type: 'POST',
		url: 'Home/AddRoom',
		data: roomName,
		success: (data) => {
			alert('success');
			console.log(data);
		},
	});
}
